package pt.diogojg.chessclock.utils

import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

/**
 * Created by Diogo Garcia
 */
class Utils {
    companion object {
        fun getFormattedTime(totalSecs: Long): String {
            val minutes = (totalSecs % 3600) / 60;
            val seconds = totalSecs % 60;

            return String.format("%02d:%02d", minutes, seconds);
        }
    }
}
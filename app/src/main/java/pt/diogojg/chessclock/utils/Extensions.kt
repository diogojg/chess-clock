package pt.diogojg.chessclock.utils

import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

/**
 * Created by Diogo Garcia
 */

fun View.shake() {
    YoYo.with(Techniques.Swing)
            .duration(700)
            .playOn(this)
}
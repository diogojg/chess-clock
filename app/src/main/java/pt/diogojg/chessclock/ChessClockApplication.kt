package pt.diogojg.chessclock

import android.app.Application
import pt.diogojg.chessclock.di.DependencyInjection

/**
 * Created by Diogo Garcia
 */
class ChessClockApplication : Application() {
    val dependencyInjection = DependencyInjection()

    companion object {
        lateinit var instance: ChessClockApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
package pt.diogojg.chessclock.impl

import io.reactivex.CompletableObserver
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import pt.diogojg.domain.CountDownTimer
import java.util.concurrent.TimeUnit

/**
 * Created by Diogo Garcia
 */
class RxCountDownTimer : CountDownTimer {
    private var initTimeMs = 0L
    private var mainTimerDisposable: Disposable? = null
    private var delayDisposable: Disposable? = null
    private val timeLeftMs = BehaviorSubject.createDefault(initTimeMs)
    private val state = BehaviorSubject.createDefault<CountDownTimer.State>(CountDownTimer.State.NotStarted)

    override fun setDuration(durationSec: Int) {
        initTimeMs = durationSec * 1000L
        timeLeftMs.onNext(initTimeMs)
    }

    override fun resume() {
        if (mainTimerDisposable == null || mainTimerDisposable!!.isDisposed) {
            state.onNext(CountDownTimer.State.Running)
            Observable.interval(100, TimeUnit.MILLISECONDS)
                    .takeUntil { timeLeftMs.value!! <= 0 }
                    .subscribe(MainTimerObserver())
        }
    }

    private inner class MainTimerObserver : Observer<Long> {
        override fun onComplete() {}

        override fun onSubscribe(d: Disposable) {
            mainTimerDisposable = d
        }

        override fun onNext(t: Long) {
            timeLeftMs.onNext(timeLeftMs.value!! - 100)

            if (timeLeftMs.value!! <= 0) state.onNext(CountDownTimer.State.Ended)
        }

        override fun onError(e: Throwable) {}
    }

    override fun pause() {
        dispose()
        if (state.value == CountDownTimer.State.Running)
            state.onNext(CountDownTimer.State.Paused)
    }

    override fun reset() {
        dispose()
        state.onNext(CountDownTimer.State.NotStarted)
        timeLeftMs.onNext(initTimeMs)
    }

    override fun timeSeconds(): Observable<Long> {
        return timeLeftMs
                .map { (it / 1000f).toLong() }
                .distinctUntilChanged()
    }

    override fun state(): Observable<CountDownTimer.State> {
        return state
    }

    override fun resumeWithDelay(delaySec: Int) {
        Observable.timer(delaySec.toLong(), TimeUnit.SECONDS)
                .ignoreElements()
                .subscribe(DelayObserver())
    }

    private inner class DelayObserver : CompletableObserver {
        override fun onComplete() {
            resume()
        }

        override fun onSubscribe(d: Disposable) {
            delayDisposable = d
            state.onNext(CountDownTimer.State.Running)
        }

        override fun onError(e: Throwable) {}
    }

    override fun increment(seconds: Int) {
        timeLeftMs.onNext(timeLeftMs.value!! + seconds * 1000)
    }

    override fun isPaused(): Boolean {
        return state.value == CountDownTimer.State.Paused
    }

    override fun isNotStarted(): Boolean {
        return state.value == CountDownTimer.State.NotStarted
    }

    override fun isRunning(): Boolean {
        return state.value == CountDownTimer.State.Running
    }

    override fun isEnded(): Boolean {
        return state.value == CountDownTimer.State.Ended
    }

    fun dispose() {
        if (mainTimerDisposable != null && !mainTimerDisposable!!.isDisposed) mainTimerDisposable!!.dispose()
        if (delayDisposable != null && !delayDisposable!!.isDisposed) delayDisposable!!.dispose()
    }
}
package pt.diogojg.chessclock.di

import pt.diogojg.chessclock.impl.RxCountDownTimer
import pt.diogojg.chessclock.ui.clock.ClockPresenter
import pt.diogojg.domain.ClockType
import pt.diogojg.domain.screen.clock.ClockInteractor

/**
 * Created by Diogo Garcia
 */
class DependencyInjection {

    fun newClockPresenter(clockType: ClockType): ClockPresenter {
        return ClockPresenter(newClockInteractor(clockType))
    }

    private fun newClockInteractor(clockType: ClockType): ClockInteractor {
        return ClockInteractor(RxCountDownTimer(), RxCountDownTimer(), clockType)
    }
}
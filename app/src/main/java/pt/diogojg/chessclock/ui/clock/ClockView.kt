package pt.diogojg.chessclock.ui.clock

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable
import pt.diogojg.domain.ClockType
import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.clock.ClockViewState

/**
 * @author Diogo Garcia
 */
interface ClockView : MvpView {
    fun resumePlayerClockIntent(): Observable<Player>

    fun resetIntent(): Observable<Unit>

    fun pauseIntent(): Observable<Unit>

    fun resumeIntent(): Observable<Unit>

    fun changeClockTypeIntent(): Observable<ClockType>

    fun render(state: ClockViewState)
}
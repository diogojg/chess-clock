package pt.diogojg.chessclock.ui.settings

import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import android.view.Window
import pt.diogojg.domain.ClockType


/**
 * Created by Diogo Garcia
 */
class SettingsDialogFactory {
    companion object {
        fun create(context: Context, listener: SettingsView.Listener, currentSettings: ClockType): Dialog {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = SettingsView(context)
            view.setListener(listener)
            view.setSettings(currentSettings)
            dialog.setContentView(view)
            dialog.window.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            return dialog
        }
    }
}
package pt.diogojg.chessclock.ui.clock

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import pt.diogojg.domain.screen.clock.ClockInteractor
import pt.diogojg.domain.screen.clock.ClockViewState

/**
 * @author Diogo Garcia
 */
class ClockPresenter(private val interactor: ClockInteractor) : MviBasePresenter<ClockView, ClockViewState>() {

    private val compositeDisposable = CompositeDisposable()

    override fun bindIntents() {
        val resumePlayerClockIntent = intent(ClockView::resumePlayerClockIntent)
                .flatMapCompletable { interactor.stopPlayerTimer(it) }

        val pauseIntent = intent(ClockView::pauseIntent)
                .flatMapCompletable { interactor.pause() }

        val resumeIntent = intent(ClockView::resumeIntent)
                .flatMapCompletable { interactor.resume() }

        val resetIntent = intent(ClockView::resetIntent)
                .flatMapCompletable { interactor.reset() }

        val changeClockTypeIntent = intent(ClockView::changeClockTypeIntent)
                .flatMapCompletable { interactor.changeClockType(it) }

        compositeDisposable.addAll(
                resumePlayerClockIntent.subscribe(),
                pauseIntent.subscribe(),
                resetIntent.subscribe(),
                resumeIntent.subscribe(),
                changeClockTypeIntent.subscribe()
        )

        val viewState = interactor.viewState()
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())

        subscribeViewState(viewState, ClockView::render)
    }

    override fun unbindIntents() {

    }
}
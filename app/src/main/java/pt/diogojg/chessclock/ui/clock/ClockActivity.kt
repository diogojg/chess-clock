package pt.diogojg.chessclock.ui.clock

import android.app.Dialog
import android.os.Bundle
import android.support.transition.TransitionManager
import android.support.v4.content.ContextCompat
import android.view.View.*
import android.view.WindowManager
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding2.view.clicks
import com.robinhood.ticker.TickerUtils
import com.robinhood.ticker.TickerView
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_clock.*
import pt.diogojg.chessclock.ChessClockApplication
import pt.diogojg.chessclock.R
import pt.diogojg.chessclock.ui.settings.SettingsDialogFactory
import pt.diogojg.chessclock.ui.settings.SettingsView
import pt.diogojg.chessclock.utils.Utils
import pt.diogojg.chessclock.utils.shake
import pt.diogojg.domain.ClockType
import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.ClockState
import pt.diogojg.domain.screen.clock.ClockViewState
import rm.com.youtubeplayicon.PlayIconDrawable


class ClockActivity : MviActivity<ClockView, ClockPresenter>(), ClockView {
    private val clockSettingsSubject = BehaviorSubject.create<ClockType>()
    private var dialog: Dialog? = null
    private val playPauseButtonState = PublishSubject.create<PlayIconDrawable.IconState>()
    internal lateinit var playPauseDrawable: PlayIconDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clock)
        makeActivityFullScreen()
        player1Button.setCharacterLists(TickerUtils.provideNumberList())
        player2Button.setCharacterLists(TickerUtils.provideNumberList())

        settingsButton.setOnClickListener {
            dialog = SettingsDialogFactory.create(this, SettingsListener(), clockSettingsSubject.value!!)
            dialog?.show()
        }
        clockSettingsSubject.onNext(ClockType.FischerBlitz(1))
        setupPlayPauseButton()
    }

    private fun makeActivityFullScreen() {
        window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_IMMERSIVE or
                SYSTEM_UI_FLAG_FULLSCREEN or
                SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                SYSTEM_UI_FLAG_IMMERSIVE_STICKY

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    private inner class SettingsListener : SettingsView.Listener {
        override fun onApplySettings(settings: ClockType) {
            clockSettingsSubject.onNext(settings)
            dialog?.dismiss()
        }
    }

    private fun setupPlayPauseButton() {
        playPauseDrawable = PlayIconDrawable.builder()
                .withDuration(200)
                .withColor(ContextCompat.getColor(this, R.color.button_tint))
                .withInitialState(PlayIconDrawable.IconState.PAUSE)
                .into(playPauseButton)
        playPauseButtonState.onNext(PlayIconDrawable.IconState.PAUSE)
        playPauseButton.setOnClickListener {
            playPauseDrawable.toggle(true)
            val nextIconState = if (playPauseDrawable.iconState == PlayIconDrawable.IconState.PLAY) PlayIconDrawable.IconState.PAUSE else PlayIconDrawable.IconState.PLAY
            playPauseButtonState.onNext(nextIconState)
        }
    }

    override fun createPresenter(): ClockPresenter {
        return ChessClockApplication.instance
                .dependencyInjection
                .newClockPresenter(clockSettingsSubject.value!!)
    }

    //region intents
    override fun resumePlayerClockIntent(): Observable<Player> {
        return player1Button.clicks().map { Player.ONE }
                .mergeWith(player2Button.clicks().map { Player.TWO })
    }

    override fun resetIntent(): Observable<Unit> {
        return resetButton.clicks()
    }

    override fun pauseIntent(): Observable<Unit> {
        return playPauseButtonState
                .filter { it == PlayIconDrawable.IconState.PLAY }
                .map { Unit }
    }

    override fun resumeIntent(): Observable<Unit> {
        return playPauseButtonState
                .filter { it == PlayIconDrawable.IconState.PAUSE }
                .map { Unit }
    }

    override fun changeClockTypeIntent(): Observable<ClockType> {
        return clockSettingsSubject.filter { !isRestoringViewState }
    }
    //endregion

    //region render
    private val clockActivityRenderer = ClockActivityRenderer(this)


    override fun render(state: ClockViewState) {
        clockActivityRenderer.render(state)
    }
    //endregion
}

package pt.diogojg.chessclock.ui.settings

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.transition.TransitionManager
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.view_settings.view.*
import pt.diogojg.chessclock.R
import pt.diogojg.domain.ClockType
import java.lang.IllegalStateException

/**
 * Created by Diogo Garcia
 */
class SettingsView : ConstraintLayout {
    private var listener: Listener? = null
    private var clockType: ClockType? = null

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_settings, this)
        setupSpinner()
        durationPicker.maxValue = 30
        durationPicker.minValue = 1
        bonusPicker.maxValue = 10
        bonusPicker.minValue = 1

        val padding = resources.getDimensionPixelSize(R.dimen.dialog_padding)
        setPadding(padding, padding, padding, padding)

        durationPicker.setOnValueChangedListener { _, _, _ ->
            clockType = getCurrentSettingsFromLayout()
            updateView()
        }

        bonusPicker.setOnValueChangedListener { _, _, _ ->
            clockType = getCurrentSettingsFromLayout()
            updateView()
        }

        applyButton.setOnClickListener { listener?.onApplySettings(getCurrentSettingsFromLayout()) }
    }

    private fun setupSpinner() {
        ArrayAdapter.createFromResource(
                context,
                R.array.clock_types,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            clockTypeSpinner.adapter = adapter
        }
        clockTypeSpinner.onItemSelectedListener = spinnerListener
    }

    private val spinnerListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            clockType = getCurrentSettingsFromLayout()
            updateView()
        }

        override fun onNothingSelected(p0: AdapterView<*>?) {}
    }

    fun setSettings(clockType: ClockType) {
        this.clockType = clockType
        updateView()
    }

    private fun updateView() {
        val clockType = clockType
        TransitionManager.beginDelayedTransition(this@SettingsView)

        when (clockType) {
            is ClockType.FischerBlitz -> {
                clockTypeSpinner.setSelection(0, true)
                durationPicker.value = clockType.minutesPerPlayer
                bonusPicker.visibility = View.GONE
                bonusLabel.visibility = View.GONE
            }
            is ClockType.DelayBullet -> {
                clockTypeSpinner.setSelection(1, true)
                durationPicker.value = clockType.minutesPerPlayer
                bonusPicker.value = clockType.delaySec
                bonusPicker.visibility = View.VISIBLE
                bonusLabel.visibility = View.VISIBLE
            }
            is ClockType.Fischer -> {
                clockTypeSpinner.setSelection(2, true)
                durationPicker.value = clockType.minutesPerPlayer
                bonusPicker.value = clockType.bonusSec
                bonusPicker.visibility = View.VISIBLE
                bonusLabel.visibility = View.VISIBLE
            }
        }
    }

    private fun getCurrentSettingsFromLayout(): ClockType {
        return when (clockTypeSpinner.selectedItem as String) {
            resources.getString(R.string.fischer_blitz) -> ClockType.FischerBlitz(durationPicker.value)
            resources.getString(R.string.delay_bullet) -> ClockType.DelayBullet(durationPicker.value, bonusPicker.value)
            resources.getString(R.string.fischer) -> ClockType.Fischer(durationPicker.value, bonusPicker.value)
            else -> throw IllegalStateException("Unknown clock type: ${clockTypeSpinner.selectedItem}")
        }
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }


    interface Listener {
        fun onApplySettings(settings: ClockType)
    }
}
package pt.diogojg.chessclock.ui.clock

import android.support.transition.TransitionManager
import android.support.v4.content.ContextCompat
import android.view.View
import com.robinhood.ticker.TickerView
import kotlinx.android.synthetic.main.activity_clock.*
import pt.diogojg.chessclock.R
import pt.diogojg.chessclock.utils.Utils
import pt.diogojg.chessclock.utils.shake
import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.ClockState
import pt.diogojg.domain.screen.clock.ClockViewState
import rm.com.youtubeplayicon.PlayIconDrawable

/**
 * Created by Diogo Garcia
 */
class ClockActivityRenderer(private val activity: ClockActivity) {
    private var lastState: ClockViewState? = null

    fun render(state: ClockViewState) {
        TransitionManager.beginDelayedTransition(activity.rootView)
        updatePausePlayButtonVisibility(state)
        updatePlayersButtonsState(state)
        lastState = state
    }

    private fun updatePausePlayButtonVisibility(state: ClockViewState) {
        val clockState = state.clockState
        if (clockState == ClockState.Paused) {
            setStateOfPlayPauseDrawable(PlayIconDrawable.IconState.PLAY)
            activity.playPauseButton.visibility = View.VISIBLE
        } else if (clockState == ClockState.Running) {
            setStateOfPlayPauseDrawable(PlayIconDrawable.IconState.PAUSE)
            activity.playPauseButton.visibility = View.VISIBLE
        } else if (clockState == ClockState.NotStarted || clockState is ClockState.Ended) {
            activity.playPauseButton.visibility = View.INVISIBLE
        }
    }

    private fun setStateOfPlayPauseDrawable(state: PlayIconDrawable.IconState) {
        if (activity.playPauseDrawable.iconState != state)
            activity.playPauseDrawable.animateToState(state)
    }

    private fun updatePlayersButtonsState(state: ClockViewState) {
        val clockState = state.clockState
        val player1Button = activity.player1Button
        val player2Button = activity.player2Button

        player1Button.text = Utils.getFormattedTime(state.player1Sec)
        player2Button.text = Utils.getFormattedTime(state.player2Sec)

        if (clockState == ClockState.Paused || clockState is ClockState.Ended) {
            player1Button.isEnabled = false
            player2Button.isEnabled = false
        } else if (state.playerTurn == Player.UNKNOWN) {
            player1Button.isEnabled = true
            player2Button.isEnabled = true
        } else {
            player1Button.isEnabled = state.playerTurn == Player.ONE
            player2Button.isEnabled = state.playerTurn == Player.TWO
        }

        if (clockState is ClockState.Ended) {
            when (clockState.playerOutOfTime) {
                Player.ONE -> {
                    applyEndedStyle(player1Button)
                    applyStandardStyle(player2Button)

                    if (lastState?.clockState !is ClockState.Ended)
                        player1Button.shake()
                }
                Player.TWO -> {
                    applyStandardStyle(player1Button)
                    applyEndedStyle(player2Button)

                    if (lastState?.clockState !is ClockState.Ended)
                        player2Button.shake()
                }
            }
        } else {
            applyStandardStyle(player1Button)
            applyStandardStyle(player2Button)
        }
    }

    private fun applyEndedStyle(playerButton: TickerView) {
        playerButton.setBackgroundResource(R.drawable.player_button_background_ended)
        playerButton.textColor = ContextCompat.getColor(activity, android.R.color.white)
    }

    private fun applyStandardStyle(playerButton: TickerView) {
        playerButton.setBackgroundResource(R.drawable.player_button_background)
        playerButton.textColor = ContextCompat.getColor(activity, R.color.time_text_color)
    }
}
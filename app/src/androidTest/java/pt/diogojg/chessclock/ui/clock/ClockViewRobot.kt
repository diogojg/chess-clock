package pt.diogojg.chessclock.ui.clock

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import org.junit.Assert
import pt.diogojg.domain.ClockType
import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.clock.ClockViewState
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * @author Diogo Garcia
 */
class ClockViewRobot(presenter: ClockPresenter) {
    private val resumePlayerClockSubject = PublishSubject.create<Player>()
    private val resetSubject = PublishSubject.create<Unit>()
    private val pauseSubject = PublishSubject.create<Unit>()
    private val resumeSubject = PublishSubject.create<Unit>()
    private val changeClockTypeSubject = PublishSubject.create<ClockType>()
    private val renderEvents = ArrayList<ClockViewState>()
    private val renderEventSubject = ReplaySubject.create<ClockViewState>()

    private val view = object : ClockView {
        override fun resumePlayerClockIntent(): Observable<Player> = resumePlayerClockSubject

        override fun resetIntent(): Observable<Unit> = resetSubject

        override fun pauseIntent(): Observable<Unit> = pauseSubject

        override fun resumeIntent(): Observable<Unit> = resumeSubject

        override fun changeClockTypeIntent(): Observable<ClockType> = changeClockTypeSubject

        override fun render(state: ClockViewState) {
            renderEvents.add(state)
            renderEventSubject.onNext(state)
        }
    }

    fun fireResumePlayerClockIntent(player: Player) {
        resumePlayerClockSubject.onNext(player)
    }

    fun fireResetIntent() {
        resetSubject.onNext(Unit)
    }

    fun firePauseIntent() {
        pauseSubject.onNext(Unit)
    }

    fun fireResumeIntent() {
        resetSubject.onNext(Unit)
    }


    fun fireChangeClockTypeIntent(clockType: ClockType) {
        changeClockTypeSubject.onNext(clockType)
    }

    init {
        presenter.attachView(view)
    }

    fun assertViewStateRendered(vararg expectedHomeViewStates: ClockViewState) {
        val eventsCount = expectedHomeViewStates.size
        renderEventSubject.take(eventsCount.toLong())
                .timeout(10L, TimeUnit.SECONDS)
                .blockingSubscribe()

        /*
    // Wait for few milli seconds to ensure that no more render events have occurred
    // before finishing the test and checking expectations (asserts)
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    */

        if (renderEventSubject.values.size > eventsCount) {
            Assert.fail("Expected to wait for "
                    + eventsCount
                    + ", but there were "
                    + renderEventSubject.values.size
                    + " Events in total, which is more than expected: "
                    + arrayToString(renderEventSubject.values))
        }

        Assert.assertEquals(Arrays.asList(expectedHomeViewStates), renderEvents)
    }

    private fun arrayToString(array: Array<Any>): String {
        val buffer = StringBuffer()
        for (o in array) {
            buffer.append(o.toString())
            buffer.append("\n")
        }

        return buffer.toString()
    }
}
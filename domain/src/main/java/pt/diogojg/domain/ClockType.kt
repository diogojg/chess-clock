package pt.diogojg.domain

/**
 * Created by Diogo Garcia
 */
abstract class ClockType(open val minutesPerPlayer: Int) {
    data class FischerBlitz(override val minutesPerPlayer: Int) : ClockType(minutesPerPlayer) {
        override fun toString(): String {
            return "Fischer Blitz $minutesPerPlayer|0"
        }
    }

    data class DelayBullet(override val minutesPerPlayer: Int, val delaySec: Int) : ClockType(minutesPerPlayer) {
        override fun toString(): String {
            return "Delay Bullet $minutesPerPlayer|$delaySec"
        }
    }

    data class Fischer(override val minutesPerPlayer: Int, val bonusSec: Int) : ClockType(minutesPerPlayer) {
        override fun toString(): String {
            return "Fischer $minutesPerPlayer|$bonusSec"
        }
    }
}
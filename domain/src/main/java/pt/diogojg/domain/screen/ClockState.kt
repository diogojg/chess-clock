package pt.diogojg.domain.screen

import pt.diogojg.domain.Player

/**
 * Created by Diogo Garcia
 */
interface ClockState {
    object Paused : ClockState
    object NotStarted : ClockState
    object Running : ClockState
    data class Ended(val playerOutOfTime: Player) : ClockState
}
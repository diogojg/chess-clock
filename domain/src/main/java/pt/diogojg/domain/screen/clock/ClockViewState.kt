package pt.diogojg.domain.screen.clock

import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.ClockState

/**
 * @author Diogo Garcia
 */
data class ClockViewState(val player1Sec: Long,
                          val player2Sec: Long,
                          val playerTurn: Player,
                          val clockState: ClockState)
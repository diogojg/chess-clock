package pt.diogojg.domain.screen.clock

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import pt.diogojg.domain.ClockType
import pt.diogojg.domain.CountDownTimer
import pt.diogojg.domain.Player
import pt.diogojg.domain.screen.ClockState
import java.lang.IllegalStateException

/**
 * @author Diogo Garcia
 */
class ClockInteractor(private val player1Timer: CountDownTimer,
                      private val player2Timer: CountDownTimer,
                      private var clockType: ClockType) {

    private val playerTurn = BehaviorSubject.create<Player>()

    init {
        playerTurn.onNext(Player.UNKNOWN)
        player1Timer.setDuration(clockType.minutesPerPlayer * 60)
        player2Timer.setDuration(clockType.minutesPerPlayer * 60)
    }

    fun stopPlayerTimer(player: Player): Completable = Completable.create {
        if (player == playerTurn.value || playerTurn.value == Player.UNKNOWN) {
            val clockType = clockType
            val nextPlayer = if (player == Player.ONE) Player.TWO else Player.ONE

            when (clockType) {
                is ClockType.FischerBlitz -> {
                    getPlayerTimer(nextPlayer).resume()
                    getPlayerTimer(player).pause()
                }
                is ClockType.DelayBullet -> {
                    getPlayerTimer(nextPlayer).resumeWithDelay(clockType.delaySec)
                    getPlayerTimer(player).pause()
                }
                is ClockType.Fischer -> {
                    getPlayerTimer(nextPlayer).resume()
                    if (!getPlayerTimer(player).isNotStarted())
                        getPlayerTimer(player).increment(clockType.bonusSec)
                    getPlayerTimer(player).pause()
                }
            }
            playerTurn.onNext(nextPlayer)
        }
        it.onComplete()
    }

    private fun getPlayerTimer(player: Player): CountDownTimer = when (player) {
        Player.ONE -> player1Timer
        Player.TWO -> player2Timer
        else -> throw IllegalStateException("Unable to get timer for player: $player")
    }

    fun pause(): Completable {
        return Completable.create {
            player1Timer.pause()
            player2Timer.pause()
            it.onComplete()
        }
    }

    fun resume(): Completable {
        return Completable.create {
            when (playerTurn.value) {
                Player.ONE -> player1Timer.resume()
                Player.TWO -> player2Timer.resume()
            }
            it.onComplete()
        }
    }

    fun reset(): Completable {
        return Completable.create {
            player1Timer.reset()
            player2Timer.reset()
            playerTurn.onNext(Player.UNKNOWN)
            it.onComplete()
        }
    }

    fun changeClockType(clockType: ClockType): Completable {
        return Completable.create {
            this.clockType = clockType
            player1Timer.setDuration(clockType.minutesPerPlayer * 60)
            player2Timer.setDuration(clockType.minutesPerPlayer * 60)
            it.onComplete()
        }
                .andThen(reset())
    }

    fun viewState(): Observable<ClockViewState> {
        val combineObs = arrayOf(
                player1Timer.timeSeconds(),
                player2Timer.timeSeconds(),
                player1Timer.state(),
                player2Timer.state(),
                playerTurn
        )
        return Observable.combineLatest(combineObs) {
            val clock1Sec = it[0] as Long
            val clock2Sec = it[1] as Long
            val clock1State = it[2] as CountDownTimer.State
            val clock2State = it[3] as CountDownTimer.State
            val playerTurn = it[4] as Player

            ClockViewState(
                    clock1Sec,
                    clock2Sec,
                    playerTurn,
                    computeClockState(clock1State, clock2State)
            )
        }
                .distinctUntilChanged()
    }

    private fun computeClockState(clock1State: CountDownTimer.State, clock2State: CountDownTimer.State): ClockState {
        return if (clock1State == CountDownTimer.State.NotStarted && clock2State == CountDownTimer.State.NotStarted) {
            ClockState.NotStarted
        } else if ((clock1State == CountDownTimer.State.Paused || clock1State == CountDownTimer.State.NotStarted) &&
                (clock2State == CountDownTimer.State.Paused || clock2State == CountDownTimer.State.NotStarted)) {
            ClockState.Paused
        } else if (clock1State == CountDownTimer.State.Ended) {
            ClockState.Ended(Player.ONE)
        } else if (clock2State == CountDownTimer.State.Ended) {
            ClockState.Ended(Player.TWO)
        } else {
            ClockState.Running
        }
    }
}
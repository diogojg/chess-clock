package pt.diogojg.domain

import io.reactivex.Observable

/**
 * @author Diogo Garcia
 */
interface CountDownTimer {
    fun setDuration(durationSec: Int)

    fun resume()

    fun resumeWithDelay(delaySec: Int)

    fun pause()

    fun reset()

    fun increment(seconds: Int)

    fun timeSeconds(): Observable<Long>

    fun state(): Observable<State>

    fun isPaused(): Boolean

    fun isNotStarted(): Boolean

    fun isRunning(): Boolean

    fun isEnded(): Boolean

    interface State {
        object Paused : State
        object NotStarted : State
        object Running : State
        object Ended : State
    }
}
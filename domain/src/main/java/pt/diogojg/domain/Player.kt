package pt.diogojg.domain

enum class Player {
    ONE,
    TWO,
    UNKNOWN
}